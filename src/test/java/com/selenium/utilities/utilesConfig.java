package com.selenium.utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import java.util.Properties;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.selenium.pageObjects.swagLabsLogin;
import junit.framework.TestResult;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.TestInfo;

public class utilesConfig
{
	public static WebDriver driver;
	public static Properties properties = new Properties();
	private static WebDriverWait wait;

	static String configBrowser = "Chrome";
	static String configUrlInicio = "https://www.saucedemo.com/";
	// static String configUrlInicio = "https://practicetestautomation.com/practice-test-login/";

	protected static ExtentReports extent;
	protected static ExtentTest test;
	public static Boolean testStatus;

	@BeforeAll
	public static void setupClass() {
		if (configBrowser.equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			options.setCapability("unexpectedAlertBehaviour", UnexpectedAlertBehaviour.IGNORE);

			driver = new ChromeDriver(options);
		}

		else {
			throw new RuntimeException("Explorador no soportado...");
		}

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(configUrlInicio);

		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("ddMMyy_HHmmss");
		String formattedDate = myDateObj.format(myFormatObj);

		extent = new ExtentReports();
		ExtentSparkReporter sparkReporter = new ExtentSparkReporter("report_" + formattedDate + ".html");
		extent.attachReporter(sparkReporter);

		test = extent.createTest(utilesConfig.class.getSimpleName(), "Descripción de la Prueba");
	}

	@BeforeEach()
    public void beforeTest() {
		testStatus = false;
		swagLabsLogin.loginUsuario("standard_user","secret_sauce");
    }

	@AfterEach
	public void afterTest(TestInfo testInfo) {
		String testName = testInfo.getDisplayName();

		if (testStatus) test.pass("La prueba " + testName + " termino exitosamente.");
		else test.fail("La prueba " + testName + " fallo.");

		driver.manage().deleteAllCookies();
		driver.get(configUrlInicio);
	}

    @AfterAll
    public static void suiteTearDown() {
		driver.quit();
		extent.flush();
	}
}
