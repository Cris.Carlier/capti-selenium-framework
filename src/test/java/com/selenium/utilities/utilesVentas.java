package com.selenium.utilities;

import com.selenium.pageObjects.swagLabsCartCheckout;
import com.selenium.pageObjects.swagLabsStore;

public class utilesVentas {

	public static void swagVentasSimple(String producto) {
		swagLabsStore.agregarProductoCarro(producto);
		swagLabsCartCheckout.carroCompra();
		swagLabsCartCheckout.checkoutInfo("User", "Test", "99999");
		swagLabsCartCheckout.checkoutOverview();
		swagLabsCartCheckout.checkoutComplete();
	}

	public static void swagVentasValidarTax(String producto) {
		swagLabsStore.agregarProductoCarro(producto);
		swagLabsCartCheckout.carroCompra();
		swagLabsCartCheckout.checkoutInfo("User", "Test", "99999");
		swagLabsCartCheckout.checkoutOverview();

		double totalItems = swagLabsCartCheckout.obtenerTotalItems();

		double tax_origen = swagLabsCartCheckout.obtenerTax();
		double tax = Math.round(tax_origen * 100.0) / 100.0;

		double total_origen = swagLabsCartCheckout.obtenerTotal();
		double total = Math.round(total_origen * 100.0) / 100.0;

		double valorTax = totalItems * 0.08;
		double validarTax = Math.round(valorTax * 100.0) / 100.0;

		double validarTotal_origen = totalItems + tax;
		double validarTotal = Math.round(validarTotal_origen * 100.0) / 100.0;

		System.out.println("Calculo Tax: $" + validarTax + ", calculo Total: $" + validarTotal);

		assert validarTax == tax : "El calculo del Tax no corresponde, esperado: " + validarTax + ", actual: " + tax;
		assert validarTotal == total : "El calculo del Total no corresponde, esperado: " + validarTotal + ", actual: " + total;

		swagLabsCartCheckout.checkoutComplete();
	}

	public static void swagVentasMultiple(String[] productos) {
		for (String producto : productos) {
			swagLabsStore.agregarProductoCarro(producto);
		}

		swagLabsCartCheckout.carroCompra();
		swagLabsCartCheckout.checkoutInfo("User", "Test", "99999");
		swagLabsCartCheckout.checkoutOverview();

		double totalItems = swagLabsCartCheckout.obtenerTotalItems();
		double tax = swagLabsCartCheckout.obtenerTax();
		double total = swagLabsCartCheckout.obtenerTotal();

		double valorTax = totalItems * 0.08;
		double validarTax = Math.round(valorTax * 100.0) / 100.0;
		double validarTotal = totalItems + tax;
		System.out.println("Calculo Tax: " + validarTax + ", calculo Total: " + validarTotal);

		assert validarTax == tax : "El calculo del Tax no corresponde, esperado: " + validarTax + ", actual: " + tax;
		assert validarTotal == total : "El calculo del Total no corresponde, esperado: " + validarTotal + ", actual: " + total;

		swagLabsCartCheckout.checkoutComplete();
	}
}
