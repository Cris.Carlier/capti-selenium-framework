package com.selenium.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import com.selenium.utilities.utilesConfig;

import java.util.concurrent.TimeUnit;

import static com.selenium.utilities.utilesConfig.testStatus;

public class utilesAcciones {

	private static final WebDriver driver = utilesConfig.driver;

	public static void click(String element) {
		try {
			Thread.sleep(1000);
			org.openqa.selenium.WebElement webElement = driver.findElement(By.xpath(element));
			webElement.click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void clickLink(String linkText) {
		try {
			Thread.sleep(1000);
			org.openqa.selenium.WebElement webElement = driver.findElement(By.linkText(linkText));
			webElement.click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void write(String element, String text) {
		try {
			Thread.sleep(1000);
			org.openqa.selenium.WebElement webElement = driver.findElement(By.xpath(element));
			webElement.clear();
			webElement.sendKeys(text);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static String read(String element) {
		try {
			Thread.sleep(1000);
			org.openqa.selenium.WebElement webElement = driver.findElement(By.xpath(element));
			return webElement.getText();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static void validateText(String element, String expected) {
		try {
			Thread.sleep(1000);
			org.openqa.selenium.WebElement webElement = driver.findElement(By.xpath(element));
			String found = webElement.getText();
			assert found.equals(expected) : "Expected text was '" + expected + "', but actual text was '" + found + "'";
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static boolean validatePresent(String element) {
		boolean result = false;
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

		if (driver.findElements(By.xpath(element)).size() != 0)
			result = true;

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return result;
	}

	public static void testOk() {
		testStatus = true;
	}
}