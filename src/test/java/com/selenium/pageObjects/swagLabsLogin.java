package com.selenium.pageObjects;

import com.selenium.utilities.utilesAcciones;

public class swagLabsLogin {
    private static final String login_title = "/html/body/div/div/div[1]";
    private static final String login_username = "//*[@id=\"user-name\"]";
    private static final String login_password = "//*[@id=\"password\"]";
    private static final String login_button = "//*[@id=\"login-button\"]";
    private static final String store_title = "/html/body/div/div/div/div[1]/div[2]/span";
    private static final String store_menu = "//*[@id=\"react-burger-menu-btn\"]";
    private static final String store_logout = "//*[@id=\"logout_sidebar_link\"]";

    public static void loginUsuario(String username, String password) {
        if (checkLogin()) logOut();

        utilesAcciones.validateText(login_title, "Swag Labs");
        utilesAcciones.write(login_username, username);
        utilesAcciones.write(login_password, password);
        utilesAcciones.click(login_button);
        utilesAcciones.validateText(store_title, "Products");
    }

    public static void logOut() {
        utilesAcciones.click(store_menu);
        utilesAcciones.click(store_logout);
    }

    public static boolean checkLogin() {
        if (utilesAcciones.validatePresent(store_title)) {
            String title = utilesAcciones.read(store_title);
            return title.equals("Products");
        }

        return false;
    }
}
