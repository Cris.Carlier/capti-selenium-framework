package com.selenium.pageObjects;

import com.selenium.utilities.utilesAcciones;

public class swagLabsCartCheckout {
    private static final String store_cart = "/html/body/div/div/div/div[1]/div[1]/div[3]/a";
    private static final String cart_title = "/html/body/div/div/div/div[1]/div[2]/span";
    private static final String cart_continue_shopping = "//*[@id=\"continue-shopping\"]";
    private static final String cart_checkout = "//*[@id=\"checkout\"]";
    private static final String checkout_title = "/html/body/div/div/div/div[1]/div[2]/span";
    private static final String checkout_first_name = "//*[@id=\"first-name\"]";
    private static final String checkout_last_name = "//*[@id=\"last-name\"]";
    private static final String checkout_postal_code = "//*[@id=\"postal-code\"]";
    private static final String checkout_mensaje_error = "/html/body/div/div/div/div[2]/div/form/div[1]/div[4]";
    private static final String checkout_continue = "//*[@id=\"continue\"]";
    private static final String checkout_cancel = "//*[@id=\"cancel\"]";
    private static final String checkout_finish = "//*[@id=\"finish\"]";
    private static final String checkout_thank_you = "/html/body/div/div/div/div[2]/h2";
    private static final String checkout_back_home = "//*[@id=\"back-to-products\"]";
    private static final String checkout_item_total = "/html/body/div/div/div/div[2]/div/div[2]/div[6]";
    private static final String checkout_tax = "/html/body/div/div/div/div[2]/div/div[2]/div[7]";
    private static final String checkout_total = "/html/body/div/div/div/div[2]/div/div[2]/div[8]";

    public static void carroCompra() {
        utilesAcciones.click(store_cart);
        utilesAcciones.validateText(cart_title, "Your Cart");
    }

    public static void checkoutInfo(String firstName, String lastName, String postalCode) {
        utilesAcciones.click(cart_checkout);
        utilesAcciones.validateText(checkout_title, "Checkout: Your Information");
        utilesAcciones.write(checkout_first_name, firstName);
        utilesAcciones.write(checkout_last_name, lastName);
        utilesAcciones.write(checkout_postal_code, postalCode);
    }

    public static void checkoutOverview() {
        utilesAcciones.click(checkout_continue);
        utilesAcciones.validateText(checkout_title, "Checkout: Overview");
    }

    public static void checkoutComplete() {
        utilesAcciones.click(checkout_finish);
        utilesAcciones.validateText(checkout_title, "Checkout: Complete!");
        utilesAcciones.validateText(checkout_thank_you, "Thank you for your order!");
        utilesAcciones.click(checkout_back_home);
    }

    public static double obtenerTotalItems() {
        float valor = Float.valueOf(utilesAcciones.read(checkout_item_total).replace("Item total: $", ""));
        System.out.println("Total Items: $" + valor);
        return valor;
    }

    public static double obtenerTax() {
        float valor = Float.valueOf(utilesAcciones.read(checkout_tax).replace("Tax: $", ""));
        System.out.println("Tax: $" + valor);
        return valor;
    }

    public static double obtenerTotal() {
        float valor = Float.valueOf(utilesAcciones.read(checkout_total).replace("Total: $", ""));
        System.out.println("Total: $" + valor);
        return valor;
    }
}