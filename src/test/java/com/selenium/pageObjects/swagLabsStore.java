package com.selenium.pageObjects;

import com.selenium.utilities.utilesConfig;
import com.selenium.utilities.utilesAcciones;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;


public class swagLabsStore {
    private static final String product_title = "/html/body/div/div/div/div[2]/div/div/div[2]/div[1]";
    private static final String product_add_to_cart = "//button[contains(text(),\"Add to cart\")]";
    private static final String product_remove_cart = "//*[@id=\"remove-sauce-labs-backpack\"]";
    private static final String product_back_to_products = "//*[@id=\"back-to-products\"]";

    public static void agregarProductoCarro(String producto) {
        WebDriver driver = utilesConfig.driver;
        org.openqa.selenium.WebElement elementoProducto = driver.findElement(By.linkText(producto));
        elementoProducto.click();

        utilesAcciones.validateText(product_title, producto);
        if (utilesAcciones.validatePresent(product_remove_cart))
            utilesAcciones.click(product_remove_cart);

        utilesAcciones.click(product_add_to_cart);
        utilesAcciones.click(product_back_to_products);
    }

    public static void eliminarProductoCarro(String producto) {
        WebDriver driver = utilesConfig.driver;
        org.openqa.selenium.WebElement elementoProducto = driver.findElement(By.linkText(producto));
        elementoProducto.click();

        utilesAcciones.validateText(product_title, producto);
        utilesAcciones.click(product_remove_cart);
        utilesAcciones.click(product_back_to_products);
    }
}