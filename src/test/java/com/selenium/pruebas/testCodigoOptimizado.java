package com.selenium.pruebas;

import com.selenium.utilities.utilesAcciones;
import com.selenium.utilities.utilesConfig;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class testCodigoOptimizado extends utilesConfig
{
    WebDriver driver = utilesConfig.driver;

    @Test
    public void test_sample() {
        // Ingresamos datos y hacemos login
        utilesAcciones.write("//*[@id=\"user-name\"]", "standard_user");
        utilesAcciones.write("//*[@id=\"password\"]", "secret_sauce");
        utilesAcciones.click("//*[@id=\"login-button\"]");

        // Buscamos elemento en el catalogo y lo seleccionamos
        try {
            org.openqa.selenium.WebElement elementoProducto = driver.findElement(By.linkText("Sauce Labs Backpack"));
            Thread.sleep(1000);
            elementoProducto.click();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // Lo agregamos al carro y volvemos al catalogo
        utilesAcciones.click("//button[contains(text(),\"Add to cart\")]");
        utilesAcciones.click("//*[@id=\"back-to-products\"]");

        // Click en carro de compra
        utilesAcciones.click("/html/body/div/div/div/div[1]/div[1]/div[3]/a");

        // Click en checkout
        utilesAcciones.click("//*[@id=\"checkout\"]");

        // Datos comprador
        utilesAcciones.write("//*[@id=\"first-name\"]", "Juan");
        utilesAcciones.write("//*[@id=\"last-name\"]", "Perez");
        utilesAcciones.write("//*[@id=\"postal-code\"]", "83000");

        // Click en continue
        utilesAcciones.click("//*[@id=\"continue\"]");

        // Click en finish
        utilesAcciones.click("//*[@id=\"finish\"]");

        // Click en back to products
        utilesAcciones.click("//*[@id=\"back-to-products\"]");
    }
}