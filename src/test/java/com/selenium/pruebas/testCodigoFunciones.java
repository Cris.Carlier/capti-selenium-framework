package com.selenium.pruebas;

import com.selenium.pageObjects.swagLabsLogin;
import com.selenium.utilities.utilesAcciones;
import com.selenium.utilities.utilesConfig;
import com.selenium.utilities.utilesVentas;
import com.selenium.pageObjects.swagLabsStore;
import com.selenium.pageObjects.swagLabsCartCheckout;
import org.junit.jupiter.api.Test;

public class testCodigoFunciones extends utilesConfig {

    @Test
    public void test_flujo_venta() {
        swagLabsLogin.loginUsuario("standard_user","secret_sauce");
        swagLabsStore.agregarProductoCarro("Sauce Labs Backpack");
        swagLabsCartCheckout.carroCompra();
        swagLabsCartCheckout.checkoutInfo("User", "Test", "99999");
        swagLabsCartCheckout.checkoutOverview();
        swagLabsCartCheckout.checkoutComplete();
        swagLabsLogin.logOut();
        utilesAcciones.testOk();
    }

    @Test
    public void test_venta_simple() {
        utilesVentas.swagVentasSimple("Sauce Labs Backpack");
        utilesAcciones.testOk();
    }

    @Test
    public void test_venta_validar_tax() {
        utilesVentas.swagVentasValidarTax("Sauce Labs Backpack");
        utilesAcciones.testOk();
    }
}