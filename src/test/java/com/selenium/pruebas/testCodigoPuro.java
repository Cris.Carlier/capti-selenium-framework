package com.selenium.pruebas;

import org.openqa.selenium.By;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.concurrent.TimeUnit;


public class testCodigoPuro
{
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        options.setCapability("unexpectedAlertBehaviour", UnexpectedAlertBehaviour.IGNORE);

        WebDriver driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.saucedemo.com/");

        Thread.sleep(1000);

        // Ingresamos datos y hacemos login
        WebElement username = driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        username.sendKeys("standard_user");

        Thread.sleep(1000);
        WebElement password = driver.findElement(By.xpath("//*[@id=\"password\"]"));
        password.sendKeys("secret_sauce");

        Thread.sleep(1000);
        WebElement botonLogin = driver.findElement(By.xpath("//*[@id=\"login-button\"]"));
        botonLogin.click();

        // Buscamos elemento en el catalogo y lo seleccionamos
        Thread.sleep(1000);
        WebElement elementoProducto = driver.findElement(By.linkText("Sauce Labs Backpack"));
        elementoProducto.click();

        // Lo agregamos al carro y volvemos al catalogo
        Thread.sleep(1000);
        WebElement agregarCarro = driver.findElement(By.xpath("//button[contains(text(),\"Add to cart\")]"));
        agregarCarro.click();

        Thread.sleep(1000);
        WebElement volverCatalogo = driver.findElement(By.xpath("//*[@id=\"back-to-products\"]"));
        volverCatalogo.click();

        // Click en carro de compra
        Thread.sleep(1000);
        WebElement carroCompra = driver.findElement(By.xpath("/html/body/div/div/div/div[1]/div[1]/div[3]/a"));
        carroCompra.click();

        // Click en checkout
        Thread.sleep(1000);
        WebElement checkout = driver.findElement(By.xpath("//*[@id=\"checkout\"]"));
        checkout.click();

        // Datos comprador
        Thread.sleep(1000);
        WebElement firstName = driver.findElement(By.xpath("//*[@id=\"first-name\"]"));
        firstName.sendKeys("Juan");

        Thread.sleep(1000);
        WebElement lastName = driver.findElement(By.xpath("//*[@id=\"last-name\"]"));
        lastName.sendKeys("Perez");

        Thread.sleep(1000);
        WebElement postalCode = driver.findElement(By.xpath("//*[@id=\"postal-code\"]"));
        postalCode.sendKeys("83000");

        // Click en continue
        Thread.sleep(1000);
        WebElement continueBtn = driver.findElement(By.xpath("//*[@id=\"continue\"]"));
        continueBtn.click();

        // Click en finish
        Thread.sleep(1000);
        WebElement finishBtn = driver.findElement(By.xpath("//*[@id=\"finish\"]"));
        finishBtn.click();

        // Click en back to products
        Thread.sleep(1000);
        WebElement volverCatalogoFin = driver.findElement(By.xpath("//*[@id=\"back-to-products\"]"));
        volverCatalogoFin.click();

        // Finalizamos ejecucion y cerramos la sesion
        Thread.sleep(1000);
        driver.quit();
    }
}