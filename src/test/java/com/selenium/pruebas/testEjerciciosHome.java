package com.selenium.pruebas;

import com.selenium.utilities.utilesConfig;
import org.junit.jupiter.api.Test;

public class testEjerciciosHome extends utilesConfig {

    /**
     * /!\ Recuerden /!\
     *
     * 1. Identificar el flujo a automatizar.
     * 2. Repetir los pasos e identificar los objetos.
     * 3. Aplicar las acciones y/o validaciones que correspondan (leer, validar, click, etc)
     * 4. Encapsular las acciones en los Page Objects correspondientes.
     * 5. Hacer las llamadas de las funciones en las Clases de Acciones.
     */

    @Test
    public void test_smoke_7_validar_campos_codigo_postal() {
        /* Validar Error: Postal Code is required */
    }

    @Test
    public void test_smoke_6_validar_campos_last_name() {
        /* Validar Error: Last Name is required */
    }

    @Test
    public void test_smoke_5_validar_campos_first_name() {
        /* Validar Error: First Name is required */
    }

    @Test
    public void test_smoke_venta() {
        /* Validar Error: First Name is required */
    }
}